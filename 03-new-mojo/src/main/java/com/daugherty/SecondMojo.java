package com.daugherty;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Goal which says hi.
 *
 * @deprecated Don't use!
 */
@Mojo( name = "sayHi", defaultPhase = LifecyclePhase.PROCESS_CLASSES )
public class SecondMojo
    extends AbstractMojo
{

  public void execute()
        throws MojoExecutionException
    {
      getLog().info("Hello from the sayHi Mojo.");
    }
}
